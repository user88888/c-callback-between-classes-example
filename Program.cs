using System;

public class A {
	public void method_f() {
		Console.WriteLine("From class A");
	}
}

public class B {
	public void method_f(Action arg) {
		Console.WriteLine("calling arg in B class");
		arg();
	}
}

public class Program
{
	public static void Main()
	{
		A a = new A();
		B b = new B();
		
		b.method_f(() => { a.method_f(); });
		b.method_f(() => Console.WriteLine("Hello world"));
	}
}